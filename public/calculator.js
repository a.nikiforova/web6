function updatePrice() {
  let s = document.getElementsByName("prodType");
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }
  let radioDiv = document.getElementById("radios");
  if (select.value == "3"||select.value == "4"||select.value == "5") {
    radioDiv.style.display = "block";
  }
  else {
    radioDiv.style.display = "none";
  }
  let radios = document.getElementsByName("color");
  radios.forEach(function (radio) {
    if (radio.checked) {
        let optionPrice = prices.color[radio.value];
        if (optionPrice !== undefined) {
            price += optionPrice;
        }
    }
  });
  let checkDiv = document.getElementById("checkboxes");
  if (select.value == "2"||select.value == "5") {
    checkDiv.style.display = "block";
  }
  else {
    checkDiv.style.display = "none";
  }
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    if (checkbox.checked) {
        let propPrice = prices.prodProperties[checkbox.name];
        if (propPrice !== undefined) {
            price += propPrice;
          }
    }
  });
  let prodPrice = document.getElementById("prodPrice");
  let k = document.getElementById("kol").value;
  if(k!=null){
  prodPrice.innerHTML = price * k + " rub";
  }
}


function getPrices() {
  return {
    prodTypes: [80, 100, 150, 200, 250],
color: {
option1: 10,
option2: 15,
option3: 20,
    },
    prodProperties: {
      prop1: 60,
      prop2: 50,
      prop3: 40,
      prop4: 30,
    }
  };
}


window.addEventListener('DOMContentLoaded', function (event) {
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  let s = document.getElementsByName("prodType");
  let select = s[0];
  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });
  let radios = document.getElementsByName("color");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let q = event.target;
      console.log(q.value);
      updatePrice();
    });
  });
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let a = event.target;
      console.log(a.name);
      console.log(a.value);
      updatePrice();
    });
  });

    updatePrice();
});
